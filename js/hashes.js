
import web3 from "web3";

const multihashes = require('multihashes');
const ipfsClient = require('ipfs-http-client')

// connect to ipfs daemon API server
//const ipfs = ipfsClient('localhost', '5001', { protocol: 'http' }) // leaving out the arguments will default to these values
const ipfs = ipfsClient('/ip4/127.0.0.1/tcp/5001')

export function getEthBytesFromIpfsHash(ipfsHash) {
  const bytestring = '0x' + multihashes.fromB58String(ipfsHash).toString('hex').slice(4);
  // TODO: is this how truffle-contract accepts it?
  return bytestring;
}

export function getIpfsHashFromEthBytes(ethBytes) {
  return multihashes.toB58String(multihashes.fromHexString('1220' + ethBytes));
}

export async function getTruffleContract(truffleJson) {
  const c = contract(truffleJson);
  // c.setProvider(new web3.providers.WebsocketProvider(WEBSOCKET_PROVIDER));
  c.setProvider(web3.currentProvider);
  return await glm.deployed();
}

export function getAllEvents(
  ContractInstance,
  eventName,
  pinArgs,
  eventDestination = [],
  callback = console.log,
  fromBlock = 0,
  toBlock = "latest",
) {
  const filter = { fromBlock: fromBlock, toBlock: toBlock };
  ContractInstance[eventName](filter, function(error, response) {
    if (error) {
      console.error(error);
      return;
    }
    for (let argIdx in pinArgs) {
      let arg = pinArgs[argIdx];
      let ipfsHash = getIpfsHashFromEthBytes(events.args[arg]);
      ipfs.pin.add(ipfsHash, function (err, result) {console.log(error, result);});
    }
  });

}
