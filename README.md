# ipfs-microservice

We want to pin all hashes posted to the blockchain. You specify the provider, contract, events and event arguments to listen for and pin everything you see.
```
mkdir ./staging
mkdir ./ipfs_data
export ipfs_staging=staging
export ipfs_data=ipfs_data

docker run -d --name ipfs_host -v $ipfs_staging:/export -v $ipfs_data:/data/ipfs -p 4001:4001 -p 127.0.0.1:8080:8080 -p 127.0.0.1:5001:5001 ipfs/go-ipfs:latest
docker logs -f ipfs_host
docker exec ipfs_host ipfs swarm peers
cp -r <something> $ipfs_staging
docker exec ipfs_host ipfs add -r /export/<something>
docker stop ipfs_host


Browser extension: https://chrome.google.com/webstore/detail/ipfs-companion/nibjojkomfdiaoajekhjakgkdhaomnch

var ipfsClient = require('ipfs-http-client')

// connect to ipfs daemon API server
var ipfs = ipfsClient('localhost', '5001', { protocol: 'http' }) // leaving out the arguments will default to these values


```
